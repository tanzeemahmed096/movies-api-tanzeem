const express = require("express");

const app = express();
const port = process.env.PORT || 3000;
const moviesRouter = require("./src/routes/moviesRouter");
const logsRouter = require("./src/routes/logsRouter");
const loggerModel = require("./src/model/logsModel");
const directorsRouter = require("./src/routes/directorsRouter");

app.use(express.json());
app.use("/", loggerModel.logger);
app.use("/api/movies", moviesRouter.router);
app.use("/api/logs", logsRouter.router);
app.use("/api/directors", directorsRouter.router);

app.use("/*", (req, res) => {
  res.status(404).send({ Error: "Not Found." });
});

app.listen(port, () => {
  console.log(`Listening to ${port}`);
});