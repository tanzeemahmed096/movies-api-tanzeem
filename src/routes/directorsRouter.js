const express = require("express");
const directorsController = require("../controller/directorController");

const router = express.Router();

router
  .get("/", directorsController.allDirectors)
  .get("/:directorId", directorsController.directorWithID)
  .post("/", directorsController.addDirector)
  .delete("/:directorId", directorsController.deleteDirector)
  .put("/:directorId", directorsController.updateDirector);

exports.router = router;