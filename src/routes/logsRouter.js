const express = require("express");
const logController = require("../controller/logsController");

const router = express.Router();


router.get("/", logController.logs);

exports.router = router;