const express = require("express");
const moviesController = require("../controller/moviesController");

const router = express.Router();

router
  .get("/", moviesController.allMovies)
  .get("/:movieId", moviesController.singleMovie)
  .post("/", moviesController.addMovie)
  .delete("/:movieId", moviesController.deleteMovie)
  .put("/:movieId", moviesController.updateMovie)

exports.router = router;
