const fs = require("fs").promises;
const path = require("path");

async function logs(req, res) {
  try {
    const filepath = path.join(__dirname, "../data/access.log");
    const readLogs = await fs.readFile(filepath, "utf-8");
    res.status(200).send(readLogs);
  } catch (err) {
    console.log(err);
    res.status(404).send("File Not Found");
  }
}

exports.logs = logs;
