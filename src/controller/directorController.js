const directorsModel = require("../model/directorModel");

exports.allDirectors = async (req, res) => {
  try {
    const directors = await directorsModel.allDirectors();
    res.status(200).json(directors);
  } catch (err) {
    res.status(404).send("Not Found.");
  }
};

exports.directorWithID = async (req, res) => {
  try {
    const { directorId } = req.params;
    const director = await directorsModel.directorWithID(directorId);
    res.status(200).json(director);
  } catch (err) {
    res.status(404).send("Not Found.");
  }
};

exports.addDirector = async (req, res) => {
  try {
    const director = await directorsModel.addDirector(req.body);
    res.status(200).json(director);
  } catch (err) {
    res.status(404).send("Not able to create.");
  }
};

exports.deleteDirector = async (req, res) => {
  try {
    const { directorId } = req.params;
    const director = await directorsModel.deleteDirector(directorId);
    res.status(200).json(director);
  } catch (err) {
    res.status(404).send("Not able to delete.");
  }
};

exports.updateDirector = async (req, res) => {
  try {
    const { directorId } = req.params;
    const director = await directorsModel.updateDirector(directorId, req.body);
    res.status(200).json(director);
  } catch (err) {
    res.status(404).send("Not able to update.");
  }
};
