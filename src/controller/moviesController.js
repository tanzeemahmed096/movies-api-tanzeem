const moviesModel = require("../model/moviesModel");

exports.allMovies = async (req, res) => {
  try {
    const movies = await moviesModel.moviesList();
    res.status(200).json(movies);
  } catch (err) {
    res.status(404).send("Not Found.");
  }
};

exports.singleMovie = async (req, res) => {
  try {
    const { movieId } = req.params;
    const movie = await moviesModel.singleMovie(movieId);
    res.status(200).json(movie);
  } catch (err) {
    res.status(404).send("Not Found.");
  }
};

exports.addMovie = async (req, res) => {
  try {
    const movieDetail = req.body;
    const movie = await moviesModel.addMovie(movieDetail);
    res.status(200).json(movie);
  } catch (err) {
    res.status(404).send("Not able to create.");
  }
};

exports.deleteMovie = async (req, res) => {
  try {
    const { movieId } = req.params;
    const movie = await moviesModel.deleteMovie(movieId);
    res.status(200).json(movie);
  } catch (err) {
    res.status(404).send("Not able to delete.");
  }
};

exports.updateMovie = async (req, res) => {
  try {
    const { movieId } = req.params;
    const movie = await moviesModel.updateMovieDetail(movieId, req.body);
    res.status(200).json(movie);
  } catch (err) {
    res.status(404).send("Not able to update.");
  }
};
