const fs = require("fs").promises;
const path = require("path");
const { Client } = require("pg");

async function copyData() {
  try {
    const client = new Client({
      user: "postgres",
      database: "moviesapi",
      password: "tan",
    });

    await client.connect();

    fs.readFile(path.resolve(__dirname, "../data/movies.json"), "utf-8")
      .then(async (data) => {
        data = JSON.parse(data);

        const allDirectors = data.reduce((acc, movie) => {
          acc.add(movie.Director);
          return acc;
        }, new Set());

        const promises = [];
        for (const director of allDirectors) {
          promises.push(
            client.query(
              `
                INSERT INTO directors(name)
                VALUES($1)
              `,
              [director]
            )
          );
        }

        await Promise.all(promises);
        return data;
      })
      .then((data) => {
        const promises = [];
        for (const movie of data) {
          promises.push(
            client.query(
              `
              INSERT INTO movies (title, description, runtime, genre, rating, metascore, votes, gross_earning_in_mil, director, actor, year, director_id) 
              VALUES ($1, $2 , $3 , $4 , $5 , $6 , $7 , $8 , $9 , $10 , $11 , (SELECT id FROM directors WHERE name = $12))
              `,
              [
                movie.Title,
                movie.Description,
                movie.Runtime,
                movie.Genre,
                movie.Rating,
                movie.Metascore === "NA" ? 0 : movie.Metascore,
                movie.Votes,
                movie.Gross_Earning_in_Mil === "NA"
                  ? 0
                  : movie.Gross_Earning_in_Mil,
                movie.Director,
                movie.Actor,
                movie.Year,
                movie.Director
              ]
            )
          );
        }

        return Promise.all(promises);
      })
      .then(async () => {
        await client.end();
      })
      .catch((err) => {
        console.log(err);
        client.end();
      });
  } catch (err) {
    console.log(err);
  }
}

copyData();
