const { Client } = require("pg");

async function runQuery(query, postData) {
  const client = new Client({
    user: "postgres",
    database: "moviesapi",
    password: "tan",
  });
  try {
    await client.connect();

    if (postData === undefined) {
      const { rows } = await client.query(query);

      await client.end();

      return rows;
    }
    const { rows } = await client.query(query, postData);

    await client.end();

    return rows;
  } catch (err) {
    console.log(err);
    await client.end();
    return err.message;
  }
}

async function allDirectors() {
  try {
    const directors = await runQuery("SELECT * FROM directors");
    return directors;
  } catch (err) {
    console.log(err);
    return err.message;
  }
}

async function directorWithID(directorId) {
  try {
    const director = await runQuery(
      `SELECT * FROM directors WHERE id = ${directorId}`
    );
    return director;
  } catch (err) {
    console.log(err);
    return err.message;
  }
}

async function addDirector({ name }) {
  try {
    const director = await runQuery(
      `
        INSERT INTO directors(name)
        VALUES($1)
        RETURNING *;
      `,
      [name]
    );
    return director;
  } catch (err) {
    console.log(err);
    return err.message;
  }
}

async function deleteDirector(directorId) {
  try {
    const director = await runQuery(
      `DELETE FROM directors WHERE id = ${directorId} RETURNING *;`
    );
    return director;
  } catch (err) {
    console.log(err);
    return err.message;
  }
}

async function updateDirector(directorId, { name }) {
  try {
    const director = await runQuery(
      `UPDATE directors SET name=$1 WHERE id=${directorId} RETURNING *;`,
      [name]
    );
    return director;
  } catch (err) {
    console.log(err);
    return err.message;
  }
}

module.exports = {
  allDirectors,
  directorWithID,
  addDirector,
  deleteDirector,
  updateDirector,
};
