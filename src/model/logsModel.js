const morgan = require("morgan");
const fs = require("fs");
const path = require("path");

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, "../data/access.log"),
  { flags: "a" }
);

exports.logger = morgan("combined", { stream: accessLogStream });