const { Client } = require("pg");

async function runQuery(query, postData) {
  const client = new Client({
    user: "postgres",
    database: "moviesapi",
    password: "tan",
  });
  try {
    await client.connect();

    if (postData === undefined) {
      const { rows } = await client.query(query);

      await client.end();

      return rows;
    }
    const { rows } = await client.query(query, postData);

    await client.end();

    return rows;
  } catch (err) {
    console.log(err);
    await client.end();
    return err.message;
  }
}

async function moviesList() {
  try {
    const movies = await runQuery("SELECT * FROM MOVIES");
    return movies;
  } catch (err) {
    console.log(err);
    return err.message;
  }
}

async function singleMovie(movieId) {
  try {
    const movie = await runQuery(
      `SELECT * FROM MOVIES WHERE RANK = ${movieId}`
    );
    return movie;
  } catch (err) {
    console.log(err);
    return err.message;
  }
}

async function addMovie({
  title,
  description,
  runtime,
  genre,
  rating,
  metascore,
  votes,
  gross_earning_in_mil,
  director,
  actor,
  year,
}) {
  try {
    const movie = await runQuery(
      `INSERT INTO movies(Title, Description, Runtime, Genre, Rating, Metascore, Votes, Gross_Earning_in_Mil, Director, Actor, Year)
       VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING *;`,
      [
        title,
        description,
        runtime,
        genre,
        rating,
        metascore,
        votes,
        gross_earning_in_mil,
        director,
        actor,
        year,
      ]
    );
    return movie;
  } catch (err) {
    console.log(err);
    return err.message;
  }
}

async function deleteMovie(movieId) {
  try {
    const movie = await runQuery(
      `DELETE FROM MOVIES WHERE rank = ${movieId} RETURNING *;`
    );
    return movie;
  } catch (err) {
    console.log(err);
    return err.message;
  }
}

async function updateMovieDetail(movieId, movieDetail) {
  try {
    const [movie] = await runQuery(
      `SELECT * FROM MOVIES WHERE rank = ${movieId}`
    );

    const updatedMovie = await runQuery(
      `
        UPDATE MOVIES
        SET title = $1,
        description = $2,
        runtime = $3,
        genre = $4,
        rating = $5,
        metascore = $6,
        votes = $7,
        gross_earning_in_mil = $8,
        director = $9,
        actor = $10,
        year = $11
        WHERE rank = ${movieId}
        RETURNING *;
    `,
      [
        movieDetail.title ? movieDetail.title : movie.title,
        movieDetail.description ? movieDetail.description : movie.description,
        movieDetail.runtime ? movieDetail.runtime : movie.runtime,
        movieDetail.genre ? movieDetail.genre : movie.genre,
        movieDetail.rating ? movieDetail.rating : movie.rating,
        movieDetail.metascore ? movieDetail.metascore : movie.metascore,
        movieDetail.votes ? movieDetail.votes : movie.votes,
        movieDetail.gross_earning_in_mil
          ? movieDetail.gross_earning_in_mil
          : movie.gross_earning_in_mil,
        movieDetail.director ? movieDetail.director : movie.director,
        movieDetail.actor ? movieDetail.actor : movie.actor,
        movieDetail.year ? movieDetail.year : movie.year,
      ]
    );
    return updatedMovie;
  } catch (err) {
    console.log(err);
    return err.message;
  }
}

module.exports = {
  moviesList,
  singleMovie,
  addMovie,
  deleteMovie,
  updateMovieDetail,
};
